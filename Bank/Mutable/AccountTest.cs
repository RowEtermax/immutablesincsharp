using NUnit.Framework;

namespace Bank.Mutable
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void AccountHasOwner()
        {
            var owner = "Row";
            var account = new Account(owner);
            Assert.AreEqual(owner, account.Owner);
        }

        [Test]
        public void AccountTransfersToAnother()
        {
            var from = new Account("Row");
            var to = new Account("Bob");
            
            from.Transfer(100, to);
            
            Assert.AreEqual(-100, from.Balance);
            Assert.AreEqual(100, to.Balance);
        }
    }
}