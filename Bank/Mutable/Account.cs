﻿namespace Bank.Mutable
{
    public class Account
    {
        public string Owner { get; }
        public int Balance { get; private set; }

        public Account(string owner)
        {
            Owner = owner;
            Balance = 0;
        }

        public void Transfer(int amount, Account toOther)
        {
            Balance -= amount;
            toOther.Balance += amount;
        }
    }
}