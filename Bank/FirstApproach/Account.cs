﻿namespace Bank.FirstApproach
{
    public struct Account
    {
        public string Owner { get; }
        public int Balance { get; private set; }

        public Account(string owner)
        {
            Owner = owner;
            Balance = 0;
        }

        public (Account sender, Account receiver) Transfer(int amount, Account receiver)
        {
            Balance -= amount;
            receiver.Balance += amount;
            
            return (this, receiver);
        }
    }
}
