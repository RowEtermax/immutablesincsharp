﻿using System;

namespace Bank.WithAnUpdateShorthand
{
    public struct Account
    {
        public string Owner { get; }
        public int Balance { get; }
        public string Address { get; }
        public int DebtLimit { get; }

        public Account(string owner, string address, int debtLimit) : this(owner, address, 0, debtLimit) {}

        public Account ChangeAddress(string newAddress) =>
            Update(address: newAddress);

        public Account ChangeDebtLimit(int newLimit) =>
            Update(debtLimit: newLimit);

        public (Account sender, Account receiver) Transfer(int amount, Account receiver) =>
            IsWithinLimits(amount) ? DoTransfer(amount, receiver) : Fail;

        Account(string owner, string address, int balance, int debtLimit)
        {
            Owner = owner;
            Address = address;
            Balance = balance;
            DebtLimit = debtLimit;
        }

        bool IsWithinLimits(int amount) => Balance - amount >= -DebtLimit;

        (Account, Account) DoTransfer(int amount, Account receiver) => (
            Update(balance: Balance - amount),
            receiver.Update(balance: receiver.Balance + amount)
        );

        static (Account, Account) Fail => throw new Exception("Transfer past the debt limit.");

        Account Update(string address = null, int? balance = null, int? debtLimit = null) =>
            new Account(
                Owner,
                address ?? Address,
                balance ?? Balance,
                debtLimit ?? DebtLimit
            );
    }
}
