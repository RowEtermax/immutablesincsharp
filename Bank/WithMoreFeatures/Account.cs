﻿using System;

namespace Bank.WithMoreFeatures
{
    public struct Account
    {
        public string Owner { get; }
        public int Balance { get; }
        public string Address { get; }
        public int DebtLimit { get; }

        public Account(string owner, string address, int debtLimit) : this(owner, address, 0, debtLimit) {}

        public Account ChangeAddress(string newAddress) =>
            new Account(Owner, newAddress, Balance, DebtLimit);

        public Account ChangeDebtLimit(int newLimit) =>
            new Account(Owner, Address, Balance, newLimit);

        public (Account sender, Account receiver) Transfer(int amount, Account receiver) =>
            IsWithinLimits(amount) ? DoTransfer(amount, receiver) : Fail;

        Account(string owner, string address, int balance, int debtLimit)
        {
            Owner = owner;
            Address = address;
            Balance = balance;
            DebtLimit = debtLimit;
        }

        bool IsWithinLimits(int amount) => Balance - amount >= -DebtLimit;

        (Account sender, Account receiver) DoTransfer(int amount, Account receiver) => (
            new Account(Owner, Address, Balance - amount, DebtLimit),
            new Account(receiver.Owner, receiver.Address, receiver.Balance + amount, receiver.DebtLimit)
        );

        static (Account, Account) Fail => throw new Exception("Transfer past the debt limit.");
    }
}
