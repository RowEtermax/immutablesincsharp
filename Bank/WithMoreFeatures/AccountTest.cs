using System;
using NUnit.Framework;

namespace Bank.WithMoreFeatures
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void AccountHasOwner()
        {
            var owner = "Row";
            var account = new Account(owner, "", 100);

            Assert.AreEqual(owner, account.Owner);
        }

        [Test]
        public void AccountTransfersToAnother()
        {
            var fromOwner = "Row";
            var toOwner = "Bob";
            var amount = 100;
            var limit = 100;
            var from = new Account(fromOwner, "", limit);
            var to = new Account(toOwner, "", 0);
            
            var (sender, receiver) = from.Transfer(amount, to);

            Assert.AreEqual(fromOwner, sender.Owner);
            Assert.AreEqual(-amount, sender.Balance);

            Assert.AreEqual(toOwner, receiver.Owner);
            Assert.AreEqual(amount, receiver.Balance);
        }

        [Test]
        public void AccountCanChangeAddress()
        {
            var newAddress = "Baker St. 1234";
            var account = new Account("Row", "Old St. 11", 100);

            var result = account.ChangeAddress(newAddress);
            
            Assert.AreEqual(newAddress, result.Address);
        }

        [Test]
        public void AccountCanChangeDebtLimit()
        {
            var newLimit = 200;
            var account = new Account("Row", "", 100);

            var result = account.ChangeDebtLimit(newLimit);
            
            Assert.AreEqual(newLimit, result.DebtLimit);
        }

        [Test]
        public void AccountFailsToTransferOverLimit()
        {
            var limit = 100;
            var amount = limit + 1;
            var from = new Account("Row", "", limit);
            var to = new Account("Bob", "", 0);

            Assert.Throws<Exception>(() => from.Transfer(amount, to));
        }
    }
}