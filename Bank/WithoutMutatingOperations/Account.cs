﻿namespace Bank.WithoutMutatingOperations
{
    public struct Account
    {
        public string Owner { get; }
        public int Balance { get; }

        public Account(string owner)
        {
            Owner = owner;
            Balance = 0;
        }

        Account(string owner, int balance)
        {
            Owner = owner;
            Balance = balance;
        }

        public (Account sender, Account receiver) Transfer(int amount, Account receiver) => (
            new Account(Owner, Balance - amount),
            new Account(receiver.Owner, receiver.Balance + amount)
        );
    }
}
