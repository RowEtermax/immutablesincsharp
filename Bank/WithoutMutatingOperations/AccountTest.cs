using NUnit.Framework;

namespace Bank.WithoutMutatingOperations
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void AccountHasOwner()
        {
            var owner = "Row";
            var account = new Account(owner);

            Assert.AreEqual(owner, account.Owner);
        }

        [Test]
        public void AccountTransfersToAnother()
        {
            var fromOwner = "Row";
            var toOwner = "Bob";
            var amount = 100;
            var from = new Account(fromOwner);
            var to = new Account(toOwner);
            
            var (sender, receiver) = from.Transfer(amount, to);

            Assert.AreEqual(fromOwner, sender.Owner);
            Assert.AreEqual(-amount, sender.Balance);

            Assert.AreEqual(toOwner, receiver.Owner);
            Assert.AreEqual(amount, receiver.Balance);
        }
    }
}